# Encriptacio i navegació anonima #

#VSLIDE

#### Índex

1. Navegació anònima
	- TorBrowser / tails
	- VPN
2. Metadades
	- Què són les metadades
	- Com esborrar les metadades
3. Geolocalització en dispositius mòbils
4. Encriptació
	- Què és l'encriptació i com funciona?
	- Guia bàsica de com encriptar dispositius android
	- Encriptació en l'ordinador

#VSLIDE

## Hola!
* Autoria: 2020 Hacklab Logout
* [Llicència: CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

#VSLIDE
## Roda de motivacions

«Què esperes d'aquesta sessió?»

«Per què estàs aquí?»

#VSLIDE
## Sondeig tècnic

* Mail: gmail - riseup
* OS: windows - ubuntu
* Anon: Tor, metadades, cares
* Mòbil: precaucions?

#VSLIDE
## Seguretat
* Relativa al context
* De grup
* Psico-social + física + info
* Progressiva
* Objectiu: activisme sostenible


#HSLIDE

# Navegació anonima #

#HSLIDE

# Metadades #

#VSLIDE

### Què són les metadades #
- Són dades que donen informació sobre altres dades
- Exemple de metadades en un missatge:
  - Hora
  - Ubicació
  - Dispositiu

#VSLIDE

#### Veiem un example
Fotografia adorable...

![Nens menjant gelat](Metadata/nens_gelat.jpg)

#VSLIDE
#### Metadades EXIF
... amb més informació del que sembla!
```
user@host:~$ exiftool nens_gelat.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
Camera Model Name : SM-G900V
ISO               : 64
Exif Version      : 0220
Flash             : No Flash
Aperture          : 2.2
GPS Altitude      : 18 m Below Sea Level
GPS Date/Time     : 2016:04:17 20:25:10Z
GPS Latitude      : 40 deg 42' 56.07" N
GPS Longitude     : 73 deg 50' 36.35" W
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
Shutter Speed     : 1/120
Create Date       : 2016:04:17 16:25:24.890
Focal Length      : 4.8 mm (35 mm equivalent: 31.0 mm)
```

#VSLIDE
#### Metadades EXIF: model de càmera
```
Camera Model Name : SM-G900V
```
![EXIF: Model de la càmera](Metadata/exif_camera-model.png)

#VSLIDE
#### Metadades EXIF: ubicació
```
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
```
![EXIF: Ubicació](Metadata/exif_gps.png)

#VSLIDE

# Com eliminar les metadades

#VSLIDE

## Esborrar metadades des d'un ordinador ##

- [MAT2](https://0xacab.org/jvoisin/mat2)([Web](https://matweb.info/), [Tails](https://tails.boum.org/doc/sensitive_documents/metadata/index.en.html), altres Linux per consola)
- [jExiftoolGui](https://github.com/hvdwolf/jExifToolGUI/releases) (Linux/Windows/Mac)

#VSLIDE

![Captura de la web del MAT2](Metadata/mat2-web.png)

#VSLIDE

![jExif](Metadata/jExifTool.png)

#VSLIDE
## Android ##

- ScrabledExif (F-Droid & Playstore)
- Es recomanable instalarla desde F-droid
- Signal porta una funcio incorporada, també deixa censurar cares.

#VSLIDE

![jExif](Metadata/Scrambled.jpg)

#VSLIDE
#### Resultat al borrar metadades

Després de borrar les metadades,
veiem que tota la informació compromesa ha estat esborrada.
```
user@host:~$ exiftool nens_gelat_neta.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
```

#HSLIDE

# Geolocalització en dispositius mobils #

#HSLIDE

# Encriptació # 

#HSLIDE

# Com encriptar un dispositiu android #

#VSLIDE

# Passos previs #
- Carregar el mòbil al màxim
- **!! Deixar el mòbil endollat durant tot el procés !!**
- Fer còpia de seguretat de totes les dades
- Canviar el bloqueig a tipus password
- **Recorda** Aquests passos són susceptibles de canviar, segons la versió d'Android

#VSLIDE

<div class="left">

En primer lloc anem a _Ajustes_

</div>

<div class="right">

![step1](EncryptAnd/Step1.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

#VSLIDE

<div class="left">

Dins de setting, anem a a la secció de seguretat.

</div>

<div class="right">

![step2](EncryptAnd/Step2.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

#VSLIDE

<div class="left">

Dins de seguretat busquem una secció que s'anomeni xifrat o encriptació.
Cliquem en Encriptar telèfon

</div>

<div class="right">

![step3](EncryptAnd/Step3.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>
#VSLIDE

<div class="left">

Donem al botó de Encriptar dispositiu.
Ens demanarà que confirmem la contrasenya, un cop introduïda començarà el procés d'encriptació.

</div>

<div class="right">

![step4](EncryptAnd/Step4.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

#VSLIDE

# Dispositiu encriptat #

#HSLIDE
# Com encriptar un dispositiu android (Xiaomi) #

#VSLIDE

# Passos previs #
- Carregar el mòbil al màxim
- !! Deixar el mòbil endollat durant tot el procés !!
- Fer còpia de seguretat de totes les dades
- Canviar el bloqueig a tipus password

#VSLIDE

<div class="left">

En primer lloc anem a Ajustes

</div>

<div class="right">

![xstep0](EncryptXiaomi/Step0.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE
<div class="left">

Després anem a la secció de sobre el telèfon, per tal d'activar les opcions de desenvolupador.

</div>

<div class="right">

![xstep1](EncryptXiaomi/Step1.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE
<div class="left">

Aquí premem sobre Versión MIUI, fins que aparegui un cartell confirmant-nos que les opcions de desenvolupador han estat activades

</div>

<div class="right">

![xstep2](EncryptXiaomi/Step2.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>


#VSLIDE

<div class="left">

Un cop activades les opcions de desenvolupador, tornem a la seccio de settings general. Aqui anem a "Ajustes adicionales"

</div>

<div class="right">

![xstep3](EncryptXiaomi/Step3.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>
#VSLIDE
<div class="left">

En "ajustes adicionales" anem a opcions de desenvolupador.

</div>

<div class="right">

![xstep4](EncryptXiaomi/Step4.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE
<div class="left">

- Dins de opcions de desenvolupador fem scroll fins abaix del tot.
- Seleccionem la ultima opció, "Cifra tu dispositivo usando la contraseña de bloqueo"

</div>


<div class="right">

![xstep5](EncryptXiaomi/Step5.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE

<div class="left">

- En aquesta secció activem el unic boto que hi ha.
- Ens demanará que confirmem el password del telefon, un cop fet començara la encriptació del dispositiu

</div>

<div class="right">

![xstep6](EncryptXiaomi/Step6.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>



#VSLIDE

# Dispositiu encriptat #

#HSLIDE

## Altres dispositius/dades que podem encriptar ##

- Ordinador
  - Linux
  - Mac
  - Windows
- Correu electronic

#VSLIDE

## Xifrat Linux ##

- La majoria de distribucións de linux venen amb un instal·lador al que li pots indicar que vols que el disc estigui xifrat. O xifrar nomes la teva carpeta personal.
- El sistema de xifrat es diu LUKS (cryptsetup)
- [Documentacio oficial cryptsetup](https://gitlab.com/cryptsetup/cryptsetup)
- [Guia com instalar linux mint xifrat](https://forums.linuxmint.com/viewtopic.php?t=298852)
- Alternativament es pot xifrar amb [Veracrypt](https://www.veracrypt.fr/en/Home.html) es altament recomanable, pero mes complicat

#VSLIDE

## Xifrat Mac ##
- Per xifrar el disk mac fa servir un programa anomenay FileVault
- [Documentación oficial de apple](https://support.apple.com/en-us/HT204837)

#VSLIDE

## Xifrat Windows ##
- Es pot xifrar amb bitlocker que ve instalat per defecte en windows
- [Manual oficial de microsoft](https://support.microsoft.com/en-us/windows/turn-on-device-encryption-0c453637-bc88-5f74-5105-741561aae838)
- Alternativament es pot xifrar amb [Veracrypt](https://www.veracrypt.fr/en/Home.html) es altament recomanable, pero mes complicat

#VSLIDE

## Xifrat Correu electronic ##
- El emails viatgen per internet sense xifrat, aixó significa que qualsevol persona pot llegirlos si els intercepta
- Es poden xifrar amb el programari [GPG](https://gnupg.org/) que funciona amb un sistema de claus asimetriques
- La forma mes facil de ferlo servir es instal·lant [Enigmail](https://enigmail.net/) que es un complement de [Thunderbird](https://www.thunderbird.net/en-US/)