# Index #
- [Formacio en seguretat i privacitat](/formacio-seguretat-privacitat/SLIDES.md)
- [Formacio de seguretat per sindicats combatius](/formacio-de-seguretat-digital-per-a-sindicats-combatius/SLIDES.md)
- [Inseguretat mòbil](/inseguretat-smartphone/SLIDES.md)
- [Formacio de encriptacio i navegacio anonima](/formacio-encriptacio-navegacio-anonima/SLIDES.md)
